package Command;

import java.util.Scanner;

public class Main {
    public static void doRef(String s) {
        switch (s) {
            case "Print": {
                System.out.println("Printing MethodRef");
                break;
            }
            case "Else": {
                System.out.println("Whatever MethodRef");
                break;
            }
        }
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String st = scanner.nextLine();

        LambdaCommand.LambCommamd lambdaCommand = (s) -> {
                switch (s) {
                    case "Print": {
                        System.out.println("Printing Lambda");
                        break;
                    }
                    case "Else": {
                        System.out.println("Whatever Lambds");
                        break;
                    }
                }

        };
        MethodReference.MethodRef ref = Main::doRef;
        AnonCommand anonCommand = new AnonCommand() {
            @Override
            public void doCommand(String s) {
                switch (s) {
                    case "Print": {
                        System.out.println("Printing anonCommand");
                        break;
                    }
                    case "Else": {
                        System.out.println("Whatever anonCommand");
                        break;
                    }
                }
            }
        };

        switch (st) {
            case "Lambda": {
                System.out.println("Running Lambda command");
                String commandForLambda = scanner.nextLine();
                lambdaCommand.CommandLam(commandForLambda);
            }
            case "MethodRef": {
                System.out.println("Running MethodRef command");
                String commandForRef = scanner.nextLine();
                ref.commandRef(commandForRef);
            }
            case "Anon": {
                System.out.println("Running Anon command");
                String commandForAnon = scanner.nextLine();
               anonCommand.doCommand(commandForAnon);
            }
            case "Object": {
                System.out.println("Running Object command");
                String commandObj = scanner.nextLine();
                Command command = new Command();
                command.doCommand(commandObj);
            }
        }
    }
}
