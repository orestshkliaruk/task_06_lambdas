package Command;

public interface MethodReference {

    @FunctionalInterface
    interface MethodRef{
        void commandRef(String s);
    }

}
