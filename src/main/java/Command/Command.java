package Command;

public class Command {
    public void doCommand(String s){
        switch (s) {
            case "Print": {
                System.out.println("Printing Object command");
                break;
            }
            case "Else": {
                System.out.println("Whatever Object command");
                break;
            }
        }
    }
}
