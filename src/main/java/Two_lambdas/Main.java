package Two_lambdas;

public class Main {
    public static void main(String[] args) {

        Claz.homeworkable lambdaReturnBiggest = (a, b, c) -> {
            if ((a > b) && (a > c)) {
                return a;
            } else if ((b > a) && (b < c)) {
                return b;
            }
            return c;
        };
        System.out.println(lambdaReturnBiggest.function(35, 25, 90));
        Claz.homeworkable lambdaRetAverage = (a, b, c) -> {
            double aver = (a + b + c) / 3;
            int res = (int) aver;
            return res;
        };
        System.out.println(lambdaRetAverage.function(35, 25, 45));





    }
}
