package Streams;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Collection;
import java.util.Optional;
import java.util.OptionalInt;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {
        int[] array = getRandomArray(20);
        printArray(array);

        IntStream stream = Arrays.stream(array);

        System.out.println("\nMax of array:");
        System.out.println(findMax(array));
        System.out.println("Average: ");
        System.out.println(findAverage(array));
        System.out.println("Min of array: ");
        System.out.println(findMin(array));
        System.out.println("Sum of array with sum: ");
        System.out.println(findSumWithSum(array));
        System.out.println("Arr multiplied by 2: ");
        multiplyBy2AndPrint(array);
        System.out.println("Arr sum with reduce: ");
        System.out.println(findSumWithReduce(array));
        printArray(countAboveAverage(array));
    }

    public static int[] countAboveAverage(int[] array){
        int sum = Arrays.stream(array).reduce((i,l)->i+l).getAsInt();
        int[] result = Arrays.stream(array).filter(i-> i>(sum/array.length)).toArray();
        return result;
    }

    public static void multiplyBy2AndPrint(int[] array){
        Arrays.stream(array)
                .map(i->i*2)
                .forEach(System.out::println);
    }

    public static int findSumWithSum(int[] array){
        return Arrays.stream(array).sum();
    }

    public static int findSumWithReduce(int[] array){
       OptionalInt res = Arrays.stream(array).reduce((i, n)->i+n);
       return res.getAsInt();
    }

    public static int findMax(int[] array){
        int result;
        result = Arrays.stream(array)
                .max()
                .getAsInt();
        return result;
    }

    public static int findMin(int[] array){
        return Arrays.stream(array)
                .min()
                .getAsInt();
    }

    public static int findAverage(int[] array){
        int result = Arrays.stream(array)
                .sum();
       // System.out.println("sum of arr: "+ result);
        return (int)result/array.length;
    }

    public static int[] getRandomArray(int sizeOfArray) {
        int[] arr = new int[sizeOfArray];
        for (int i = 0; i < arr.length; i++) {

            arr[i] = (int) (Math.random() * 30);
        }
        return arr;
    }

    public static void printArray(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i]+".");
        }
    }

    @FunctionalInterface
    interface Average{
        public int find(int[] array);
    }

}
