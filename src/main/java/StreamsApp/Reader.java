package StreamsApp;

import java.util.Scanner;

public class Reader {
    StringBuilder input = new StringBuilder();

    public StringBuilder read() {
        Scanner scanner = new Scanner(System.in);
        String s;
        while (true) {
            s = scanner.nextLine();
            if (s.equals("")) {
                break;
            } else {
                input.append(s+" ");
            }
        }
        return input;
    }


}
