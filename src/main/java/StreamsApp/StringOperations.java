package StreamsApp;

import com.sun.deploy.util.ArrayUtil;

import java.util.*;
import java.util.stream.Collectors;

public class StringOperations {

    String[] sArray;
    int numberOfUniqueWords = 0;
    Map<String, Integer> mapOfWords = new HashMap<>();
    String[] arrOfUnique;
    String[] arrOfUniqueWords;
    Set<String> setOfUniqueWords = new HashSet<>();

    public void doHomework(StringBuilder input) {
        String s = input.toString();
        sArray = s.split(" ");
        numberOfUniqueWords = 0;
        mapOfWords = new HashMap<>();

        System.out.println("Number of entered words: " + sArray.length);

        for (int i = 0; i < sArray.length; i++) {
            setOfUniqueWords.add(sArray[i]);
        }
        numberOfUniqueWords = setOfUniqueWords.size();
        System.out.println("Number Of Unique Words: " + numberOfUniqueWords);

        arrOfUniqueWords = setOfUniqueWords.toArray(new String[setOfUniqueWords.size()]);
        Arrays.sort(arrOfUniqueWords);

        System.out.println("Unique words sorted(uppercase first, sorry): ");
        print(arrOfUniqueWords);
        System.out.println("________");
        System.out.println("Occurance rate of each word: ");

        arrOfUnique = setOfUniqueWords.toArray(new String[setOfUniqueWords.size()]);

        for (int i = 0; i < arrOfUnique.length; i++) {
            mapOfWords.put(arrOfUnique[i], 0);
        }
        Map<String, Integer> temtMapOfWords = new HashMap<>();
        for (String value : mapOfWords.keySet()) {
            int count = 0;
            for (int i = 0; i < sArray.length; i++) {
                if (sArray[i].equals(value)) {
                    count++;
                }
            }
            temtMapOfWords.put(value, count);
        }


        for (String name : temtMapOfWords.keySet()) {
            String key = name.toString();
            String value = temtMapOfWords.get(name).toString();
            System.out.println(key + " = " + value);
        }

    }

    public void doStuffWithChars(StringBuilder input) {
        String s = input.toString();
        String result = s.replaceAll("[A-Z]", "");//got the String without uppercase chars
        Character[] charactersArray =
                result.chars().mapToObj(c -> (char) c).toArray(Character[]::new);//arr of chars w\o uppers, NOT UNIQUE

        List<Character> listOfChars = Arrays.asList(charactersArray);

        Set<Character> setOfCharacters = new HashSet<>();//set of chars w\o uppers (only uniques)
        for (int i = 0; i < charactersArray.length; i++){
            setOfCharacters.add(charactersArray[i]);
        }

        Character[] uniqueCharactersArray = setOfCharacters.toArray(new  Character[setOfCharacters.size()]); //arr of unique characters


        Map<Character, Integer> tempMapOfChars = new HashMap<>(); //map with every unique char with zero counter
        for (int i = 0; i < uniqueCharactersArray.length; i++) {
            tempMapOfChars.put(uniqueCharactersArray[i], 0);
        }

        Map<Character, Integer> mapOfChars = new HashMap<>();
        for(Character value : tempMapOfChars.keySet()){
            int count = 0;
            for (int i = 0; i < charactersArray.length; i++) {
                if (charactersArray[i].equals(value)) {
                    count++;
                }
            }
            mapOfChars.put(value, count);
        }

        for (Character name : mapOfChars.keySet()) {
            String key = name.toString();
            String value = mapOfChars.get(name).toString();
            System.out.println(key + " = " + value);
        }

    }


    public static void print(String[] arr) {
        for (int i = 0; i < arr.length; i++) {
            System.out.println(arr[i]);
        }
    }


}
