package StreamsApp;

import java.util.ArrayList;

public class App {
    public static void main(String[] args) {

        Reader reader = new Reader();
       StringBuilder input= reader.read();
       // System.out.println(input);

        StringOperations operations = new StringOperations();

       // operations.doHomework(input);
        operations.doStuffWithChars(input);

    }
}
